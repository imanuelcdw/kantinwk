<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Event\Event;
/**
 * Menus Controller
 *
 * @property \App\Model\Table\MenusTable $Menus
 *
 * @method \App\Model\Entity\Menu[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MenusController extends AppController
{

    public function beforeFilter(Event $event){
         if(!$this->Auth->user()){
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
         }
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $menus = $this->Menus->find()->order(['id'=>'DESC']);
        $no = 1;

        $this->set(compact('menus','no'));

        // Breadcrumbs

        $title = "Menu";
        $subtitle = 'Daftar '. $title;
        $breadcrumbs = [
            0 => [
                'title' => $subtitle,
                'url' => Router::url(['action' => 'index']),
            ],
        ];

        $this->set(compact('title','breadcrumbs'));
    }

    /**
     * View method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $menu = $this->Menus->get($id, [
            'contain' => []
        ]);

        $this->set('menu', $menu);

        //  Breadcrumb

        $title = "Menu";
        $subtitle = "View";
        $breadcrumbs = [
            0 => [
                'title' => $title,
                'url' => Router::url(['action' => 'index']),
            ],
            1 => [
                'title' => $subtitle.' '.$title,
                'url' => Router::url(['action' => $subtitle]),
            ],
        ];

        $this->set(compact('title','breadcrumbs','subtitle'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $menu = $this->Menus->newEntity();
        if ($this->request->is('post')) {
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            // pr($menu); die;
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $this->set(compact('menu'));

        //  Breadcrumb

        $title = "Menu";
        $subtitle = "Add";
        $breadcrumbs = [
            0 => [
                'title' => $title,
                'url' => Router::url(['action' => 'index']),
            ],
            1 => [
                'title' => $subtitle.' '.$title,
                'url' => Router::url(['action' => $subtitle]),
            ],
        ];

        $this->set(compact('title','breadcrumbs','subtitle'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $menu = $this->Menus->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menu = $this->Menus->patchEntity($menu, $this->request->getData());
            // dd($menu);die;
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }
        $this->set(compact('menu'));

        //  Breadcrumb

        $title = "Menu";
        $subtitle = "Edit";
        $breadcrumbs = [
            0 => [
                'title' => $title,
                'url' => Router::url(['action' => 'index']),
            ],
            1 => [
                'title' => $subtitle.' '.$title,
                'url' => Router::url(['action' => $subtitle]),
            ],
        ];

        $this->set(compact('title','breadcrumbs','subtitle'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);
        $menu = $this->Menus->get($id);
        if ($this->Menus->delete($menu)) {
            $this->Flash->success(__('The menu has been deleted.'));
        } else {
            $this->Flash->error(__('The menu could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function confirmation($id = null){
        $this->Flash->warning(__($id));

        return $this->redirect(['action' => 'index']);
    }
}
