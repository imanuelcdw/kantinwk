<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Dashboard Controller
 *
 *
 * @method \App\Model\Entity\Dashboard[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DashboardController extends AppController
{
    public function beforeFilter(Event $event){
         if(!$this->Auth->user()){
            return $this->redirect(['controller'=>'Users', 'action'=>'login']);
         }
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $menus = TableRegistry::get('Menus')->find();

        $this->set(compact('menus'));

        $title = "Dashboard";
        $subtitle = ''. $title;
        $breadcrumbs = [
            0 => [
                'title' => $subtitle,
                'url' => Router::url(['action' => 'index']),
            ],
        ];

        $this->set(compact('title','breadcrumbs'));
    }

 
}
