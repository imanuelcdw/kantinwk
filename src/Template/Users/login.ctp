    
	<div class="card">
		<div class="card-header">
			
		</div>
		<div class="card-body">
			<h1 class="text-left">Login</h1>
			<hr>

		    <?= $this->Form->create() ?>

			<div class="form-group">
				<?= $this->Form->control('username', ['class'=>'form-control','autocomplete'=>'off']); ?>
			</div>
			<div class="form-group">	
		    	<?= $this->Form->control('password', ['class'=>'form-control']); ?>
			</div>

			<?= $this->Form->button('Login',['class'=>'btn btn-primary float-right']) ?>

		    <?= $this->Form->end() ?>
		</div>
	</div>