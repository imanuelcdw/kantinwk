<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
 
    <div class="card">
        <div class="card-header">
            
        </div>
        <div class="card-body">
            <h1 class="text-left">Register</h1>
            <hr>

            <?= $this->Form->create() ?>
            <div class="form-group">
                <?= $this->Form->control('name', ['class'=>'form-control']); ?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('username', ['class'=>'form-control']); ?>
            </div>
            <div class="form-group">    
                <?= $this->Form->control('password', ['class'=>'form-control']); ?>
            </div>

            <?= $this->Form->button('Register',['class'=>'btn btn-primary float-right']) ?>

            <?= $this->Form->end() ?>
        </div>
    </div>