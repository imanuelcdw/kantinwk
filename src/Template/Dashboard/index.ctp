<?php 
function monthArray($key = null)
    {
        $array = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        if($key == null){
            return $array;
        }else{
            return $array[$key];
        }

    }
$bulan = monthArray();
$tanggal = date('d-m-Y h:i:s');
$datepick = explode(" ", $tanggal);
$split = explode("-", $datepick[0]);
$tanggal = $split[0] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[2]. ' ' . $datepick[1];
 ?>
<div class="card">
	<div class="card-header">
		Dashboard
	</div>
	<div class="card-body">
		<h1>Jumlah Menu : <?= $menus->count() ?></h1>
		<h2><?= $tanggal; ?> WIB</h2>
	</div>
</div>