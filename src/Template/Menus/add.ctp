<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Menu $menu
 */
?>
<?= $this->Form->create($menu, ['type'=>'file']) ?>
<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <?= $subtitle. ' ' .$title ?>
            </div>
            <div class="card-body">
                

                <div class="form-group"> 
                    <?= $this->Form->control('name',['class'=>'form-control','label'=>'Nama Menu']); ?>
                </div>
                <div class="form-group"> 
                    <?= $this->Form->control('type',['class'=>'form-control','label'=>'Jenis','options'=>['makanan'=>'Makanan','minuman'=>'Minuman']]); ?>
                </div>
                <div class="form-group"> 
                    <?= $this->Form->control('price',['class'=>'form-control','label'=>'Harga','type'=>'number','min'=>'0']); ?>
                </div>
                    
                <?= $this->Form->button(__('Save'),['class'=>'btn btn-primary']) ?>
                
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                Picture
            </div>
            <div class="card-body">
                               
                <img id="preview" src="#" alt="your image" class="img-fluid d-none" />
                
                <br>
                <div class="form-group">
                    <?= $this->Form->control('picture',['class'=>'form-control','label'=>'Gambar','type'=>'file']); ?>
                </div>


            </div>
        </div>
    </div>
</div>
<script>
    function readURL(input) {

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#picture").change(function() {
        $('#preview').removeClass('d-none');
      readURL(this);
    });
</script>
<?= $this->Form->end() ?>
