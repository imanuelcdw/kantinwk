<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Menu[]|\Cake\Collection\CollectionInterface $menus
 */
?>

<div class="card">
    <div class="card-header">
        Daftar Menu
    </div>
    <div class="card-body">
        <div class="clearfix">
            <div class="float-left">

            </div>
            <div class="float-right">
                <a href="<?= $this->Url->Build(['action'=>'add']) ?>" class="btn btn-primary btn-sm">Tambah Data</a>
            </div> 
        </div>    
        <br>
        
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Price</th>
                    <th>Created</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($menus as $menu): ?>
                <tr>
                    <td><?= $no++ ?></td>
                    <td><?= $menu->name ?></td>
                    <td><?= ucfirst($menu->type ) ?></td>
                    <td><?= $this->Number->format($menu->price,['before'=>'Rp.','after'=>',-']) ?></td>
                    <td><?= $menu->created->timeAgoInWords() ?></td>
                    <td>
                        <a href="<?= $this->Url->Build(['action'=>'edit',$menu->id]) ?>" class="btn btn-success btn-sm">Edit</a>

                        <a href="<?= $this->Url->Build(['action'=>'confirmation',$menu->id]) ?>" class="btn btn-danger btn-sm">Delete</a>

                        <a href="<?= $this->Url->Build(['action'=>'view',$menu->id]) ?>" class="btn btn-warning btn-sm">View</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>


