<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Menu $menu
 */
?>

<div class="row">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                <?= $subtitle. " " .$title ?>
            </div>
            <div class="card-body">
                <h1 class="display-3"><?= h($menu->name) ?></h1>
                <table class="table table-striped">
                    <tr>
                        <th scope="row" align="center"><?= __('Type') ?></th>
                     
                        <td><?= ucfirst(h($menu->type)) ?></td>
                    </tr>
                    <tr>
                        <th scope="row" align="center"><?= __('Price') ?></th>
                     
                        <td><?= $this->Number->format($menu->price,['before'=>'Rp.','after'=>',-']) ?></td>
                    </tr>
                    <tr>
                        <th scope="row" align="center"><?= __('Created') ?></th>
                     
                        <td><?= h($menu->created) ?></td>
                    </tr>
                    <tr>
                        <th scope="row" align="center"><?= __('Modified') ?></th>
                     
                        <td><?= h($menu->modified) ?></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                Picture
            </div>
            <div class="card-body">
                <div class="container <?= (empty($menu->picture)) ? '' : 'd-none' ?>" id="text">
                        <h3 class="text-center">Tidak Ada Gambar</h3>
                    </div>

                    <img id="preview" src="<?= substr($menu->picture_dir, 7).$menu->picture ?>" alt="your image" class="img-fluid <?= (empty($menu->picture)) ? 'd-none' : '' ?>" />
            </div>
        </div>
    </div>
</div>


