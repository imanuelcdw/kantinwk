<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->fetch('title') ?></title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css(['style','bootstrap']) ?>

    <?= $this->Html->script(['jquery', 'popper', 'bootstrap']) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body style="background: #fff">
    <nav class="navbar navbar-expand-lg bg-primary navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="#">DMD</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    
                    
                </ul>
                <span class="navbar-text">
                    <ul class="navbar-nav mr-auto">
                        <?php 
                            $session = $this->request->getSession();
                            $user = $session->read('Auth.User.name');
                            $here = $this->request->getRequestTarget();
                         ?>
                        <?php if($user == null): ?>
                            <li class="nav-item">
                                <a href="<?= $this->Url->Build(['action' =>'login']) ?>" class="nav-link <?= ($this->Url->Build(['action' =>'login']) == $here) ? 'active' : '' ?>">Login</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a href="<?//= $this->Url->Build(['action' =>'add']) ?>" class="nav-link <?//= ($this->Url->Build(['action' =>'add']) == $this->request->getRequestTarget()) ? 'active' : '' ?>">Register</a>
                            </li> -->
                        <?php else: ?>
                            <li class="nav-item">
                                <div class="dropdown">
                                  <a href="#" class="dropdown-toggle nav-link" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <b><?= $user ?></b>
                                  </a>
                                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item text-secondary" href="<?= $this->Url->Build(['controller'=>'Users','action'=>'logout']) ?>">Log Out</a>
                                  </div>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a href="" class="nav-link"></a>
                            </li>
                        <?php  endif; ?>
                    </ul>
                </span>
            </div>
        </div>
    </nav>
    
    <?= $this->Flash->render() ?>
    <br>
    <?php if($this->Url->Build(['action' =>'login']) != $here): ?>
        <div class="container">
            <ol class="breadcrumb bg-transparent">
                <?php foreach($breadcrumbs as $breadcrumb): ?>     
                    <li class="breadcrumb-item"><a href="<?= $breadcrumb['url'] ?>"><?= $breadcrumb['title'] ?></a></li>
                <?php endforeach; ?>
            </ol>
        </div>
    <?php endif; ?>


    <div class="container">
        <div class="row">
            <?php if($user == null): ?>
                <div class="col-lg-6 offset-3">
                    <br>
                    <?= $this->fetch('content') ?>
                </div>
            <?php else: ?>
                <div class="col-lg-4">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            Menu
                        </div>
                        <div class="card-body">
                            <div class="list-group list-group-flush" id="list-tab" role="tablist">

                              <a href="<?= $this->Url->Build(['controller'=>'Dashboard', 'action' =>'index']) ?>" class="list-group-item list-group-item-action <?= ($here == $this->Url->Build(['controller'=>'Dashboard'])) ? 'active' : '' ?>">
                                  Dashboard
                              </a>
                              <a href="<?= $this->Url->Build(['controller'=>'Menus', 'action' =>'index']) ?>" class="list-group-item list-group-item-action  <?= ($here == $this->Url->Build(['controller'=>'Menus'])) ? 'active' : '' ?>">
                                  Daftar Menu
                              </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8">
                    <br>
                    <?= $this->fetch('content') ?>
                </div>

            <?php  endif; ?>
        </div>
    </div>
    
    

    <footer>
    </footer>
    <script>
        window.setTimeout(function() {
            $("#alert").slideToggle();
        }, 3000);
    </script>
</body>
</html>
