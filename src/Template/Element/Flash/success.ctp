<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-success" role="alert" id="alert">
  <div class="container text-center">
  	<?= $message ?>
  </div>
</div>