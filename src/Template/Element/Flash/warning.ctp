<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-warning" role="alert">
	<div class="container text-center">
  	Apa anda yakin menghapus data dengan id #<?= $message ?>?
  	<a href="<?= $this->Url->Build(['action'=>'delete',$message]) ?>">Ya</a> | 
  	<a href="#" onclick="$('.alert').hide('slow')">Tidak</a>
  </div>
</div>
