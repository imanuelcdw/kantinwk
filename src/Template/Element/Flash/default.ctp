<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-primary" role="alert" id="alert">
  <div class="container text-center">
  	<?= $message ?>
  </div>
</div>
